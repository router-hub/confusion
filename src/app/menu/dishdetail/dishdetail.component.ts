import { Component, OnInit,ViewChild,Inject} from '@angular/core';
import {Dish} from "../../shared/dish";
import { DishService } from '../../services/dish.service';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { switchMap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DishComment } from '../../shared/dishcomment';
import {visibility,flyInOut,expand} from '../../animations/app.animation';
import { animation } from '@angular/animations';
@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host :{
    '[@flyInOut]': 'true',
  'style': 'display: block;'
  },
  animations: [
    flyInOut(),visibility(),expand()
  ]
})
export class DishdetailComponent implements OnInit {
  @ViewChild ('dcomment') dishCommentFormDirective;
  dish :Dish;
  errMess : string;
  dishIds:string[];
  prev:string;
  next:string;
  dishcopy:Dish;
  visibility='shown';
  dishCommentForm: FormGroup;
  dishcomment: DishComment;
  formErrors={
    'name':'',
    'comment':''
  }
  validationMessage = {
    'name':{
      'required':'Author name is required',
      'minlength':'Author name must be atleast 2 charcters long',
    },
    'comment':{'required':'Comment is required'}
  };
  constructor(private dishservice: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    @Inject('BaseURL') public BaseURL) { 
      this.createForm();
    }


    ngOnInit() {

      this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds);

      this.route.params.pipe(switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishservice.getDish(+params['id']); }))
    .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
      errmess => this.errMess = <any>errmess);
    }
    createForm():void
    {
     this.dishCommentForm = this.fb.group({
     name: ['', [Validators.required,Validators.minLength(2)]],
     rating:5,
     comment:  ['',Validators.required]
   });
   this.dishCommentForm.valueChanges.subscribe(data => this.onValueChanged( data ));

   this.onValueChanged();

   }
   onSubmit() {
    this.dishcomment = this.dishCommentForm.value;
    console.log(this.dishcomment);
    let x=new Date();
    this.dishcopy.comments.push({
      'rating' : +this.dishcomment.rating,
      'author' : this.dishcomment.name,
      'comment': this.dishcomment.comment,
      'date': String(x)

    })
    this.dishservice.putDish(this.dishcopy).subscribe(dish =>{
      this.dish=dish; this.dishcopy = dish;
    },
    errmess => { this.dish=null;this.dishcopy=null; this.errMess = <any>errmess;});
    this.dishCommentForm.reset({
      name :'',
      rating:5,
      comment:''
      });
      this.dishCommentFormDirective.resetForm();
  }
   
  onValueChanged(data?:any){
    if(!this.dishCommentForm)
    return ;
    const form = this.dishCommentForm;
    for(const field in this.formErrors)
    {
      if(this.formErrors.hasOwnProperty(field)){
        this.formErrors[field]='';
        const control = form.get(field);
        if(control && control.dirty && !control.valid){
          const message=this.validationMessage[field];
          for(const key in control.errors){
            if(control.errors.hasOwnProperty(key)){
              this.formErrors[field] +=message[key]+ ' ';
            }
          }
        }
      }
    }
  }

    goBack(): void {
      this.location.back();
    }
    setPrevNext(dishId: string) {
      const index = this.dishIds.indexOf(dishId);
      this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
      this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
    }
  

}
