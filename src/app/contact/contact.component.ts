import { Component, OnInit ,ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Feedback, ContactType } from '../shared/feedback';
import {flyInOut} from '../animations/app.animation';
import {FeedbackService} from '../services/feedback.service';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host :{
    '[@flyInOut]': 'true',
  'style': 'display: block;'
  },
  animations: [
    flyInOut()
  ]
})
export class ContactComponent implements OnInit {
  @ViewChild('fform') feedbackFormDirective;
  feedbackForm: FormGroup;
  feedback: Feedback;
  feedbackcopy: Feedback;
  formReview:boolean=false;
  feedbackReturn:boolean=false;
  contactType = ContactType;
  errMess:string;
  formErrors={
    'firstname':'',
    'lastname':'',
    'telnum':'',
    'email':''
  }

  validationMessage = {
    'firstname':{
      'required':'First name is required',
      'minlength':'First Name must be at least 2 charcters long ',
      'maxlength':'First Name cannot be more than 25 charcters long'
    },
    'lasttname':{
      'required':'Last name is required',
      'minlength':'Last Name must be at least 2 charcters long ',
      'maxlength':'Last Name cannot be more than 25 charcters long'
    },
    'telnum':{
      'required':'Tel. number is required',
      'pattern':'Tel. number must contain only numbers'
    },
    'email':{
      'required':'Email is required',
      'email':'Email not in valid format'
    },
  };
  constructor(private feedbackservice:FeedbackService,
    private fb: FormBuilder) { 
    this.createForm();
  }

  ngOnInit(): void {
    // this.feedbackservice.getFeedback().subscribe(feedback=>this.feedbackcopy=feedback);
  }
  createForm():void
  {
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required,Validators.minLength(2),Validators.maxLength(25)]],
      lastname:  ['', [Validators.required,Validators.minLength(2),Validators.maxLength(25)]],
      telnum:  ['', [Validators.required,Validators.pattern]],
      email:  ['', [Validators.required,Validators.email]],
      agree: false,
      contacttype: 'None',
      message: ''
    });
    this.feedbackForm.valueChanges.subscribe(data => this.onValueChanged( data ));

    this.onValueChanged();

  }
  onSubmit() {
    this.feedbackReturn=true;
    this.feedback = this.feedbackForm.value;
    console.log(this.feedback);
    this.feedbackcopy=this.feedback;
    this.feedbackservice.postFeedback(this.feedbackcopy).subscribe(feedback => {this.feedback=feedback;this.feedbackReturn=false;this.formReview=true;},
    errmess => {this.feedbackcopy=null;this.feedback=null; this.errMess = <any>errmess;});
    setTimeout(()=>{this.formReview=false;},10000);
    
    this.feedbackForm.reset({
      firstname :'',
      lastname :'',
      telnum:'',
      email:'',
      agree: false,
      contacttype:'None',
      message:''
      });
      this.feedbackFormDirective.resetForm();
  }

  onValueChanged(data?:any){
    if(!this.feedbackForm)
    return ;
    const form = this.feedbackForm;
    for(const field in this.formErrors)
    {
      if(this.formErrors.hasOwnProperty(field)){
        this.formErrors[field]='';
        const control = form.get(field);
        if(control && control.dirty && !control.valid){
          const message=this.validationMessage[field];
          for(const key in control.errors){
            if(control.errors.hasOwnProperty(key)){
              this.formErrors[field] +=message[key]+ ' ';
            }
          }
        }
      }
    }
  }

}
