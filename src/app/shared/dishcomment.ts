export class DishComment {
    name: string;
    rating : number;
    comment: string;
};
